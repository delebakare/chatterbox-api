from django.db import models
from django.utils import timezone

# Create your models here.

STATUS_TYPE= (
    (1, 'active- On track'),
    (2, 'active- Slow'),
    (3, 'Inactive'),
)

class Organisation(models.Model):
    name = models.CharField(max_length=255)
    created = models.DateTimeField(default=timezone.now, null=True)

    def __str__(self):
        return self.name

class Course(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name
    
class Student(models.Model):
    name = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    organisation = models.ForeignKey(Organisation, on_delete=models.CASCADE)
    total_credits = models.FloatField(default=10)
    remaining_credits = models.FloatField(default=0)
    is_certified = models.BooleanField(default=False)
    enrolled_date = models.DateTimeField(default=timezone.now, null=True)
    last_booking_date = models.DateTimeField(default=timezone.now, null=True)

    def __str__(self):
        return self.name
    
class CourseEnrolled(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    status = models.IntegerField(default=1, choices=STATUS_TYPE)
    course_enrolled_date = models.DateTimeField(default=timezone.now, null=True)

    def __str__(self):
        return self.student.name + " " + self.course.name    

