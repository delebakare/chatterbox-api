from django.contrib import admin

# Register your models here.
from .models import Student, Course, CourseEnrolled, Organisation

class StudentAdmin(admin.ModelAdmin):
	list_display = ('name','enrolled_date','last_booking_date')

admin.site.register(Student,StudentAdmin) 
admin.site.register(Course)
admin.site.register(CourseEnrolled)
admin.site.register(Organisation)


