from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url, include
from .views import *
from rest_framework.routers import DefaultRouter
from django.conf.urls import (handler400, handler403, handler404, handler500)
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from .views import error404

handler404 = error404


app_name = "api"

urlpatterns = [
    path('dashboard/', Dashboard.as_view(), name='dashboard'),
    path('studentway/<int:student_id>/', GetStudentInfo2.as_view(), name='student2'),
    
]
