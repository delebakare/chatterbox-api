from django.shortcuts import render
from datetime import datetime, timedelta
from django.http import HttpResponseRedirect, JsonResponse
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view, permission_classes
from rest_framework.exceptions import NotFound
from .models import *
from .serializers import * 


# Create your views here.

def error404(request, exception):
    raise NotFound(detail="Error 404, page not found", code=404)

def progressdata(students):
    content = {
        'on_track': 0,
        'active_slow': 0,
        'in_atcive': 0
    }
    for student in students:
        start_date = student.enrolled_date.date()
        end_date = student.last_booking_date.date()

        diff = end_date - start_date
        weeks = diff // 7
        weeks = weeks.days

        remaining_credit = student.remaining_credits
        total_credit = student.total_credits

        try:
            ans = (((remaining_credit / total_credit) * weeks) / weeks) * 100
        except ZeroDivisionError:
            ans = 0
        if ans <= 30:
            content['on_track'] += 1
        if ans > 30 and ans <= 70:
            content['active_slow'] += 1
        if ans >= 70:
            content['in_atcive'] += 1
    return content    


def singleprogress(student):
    content = {
        'on_track': 0,
        'active_slow': 0,
        'in_atcive': 0
    }
    
    start_date = student.enrolled_date.date()
    end_date = student.last_booking_date.date()

    diff = end_date - start_date
    weeks = diff // 7
    weeks = weeks.days

    remaining_credit = student.remaining_credits
    total_credit = student.total_credits

    try:
        ans = (((remaining_credit / total_credit) * weeks) / weeks) * 100
    except ZeroDivisionError:
        ans = 0
    if ans <= 30:
        content['on_track'] += 1
    if ans > 30 and ans <= 70:
        content['active_slow'] += 1
    if ans >= 70:
        content['in_atcive'] += 1
    return content    


def progresstat(id):
    content = {
        'on_track': 0,
        'active_slow': 0,
        'in_atcive': 0
    }
    student = Student.objects.get(id=id)
    start_date = student.enrolled_date.date()
    end_date = student.last_booking_date.date()

    diff = end_date - start_date
    weeks = diff // 7
    weeks = weeks.days

    remaining_credit = student.remaining_credits
    total_credit = student.total_credits

    try:
        ans = (((remaining_credit / total_credit) * weeks) / weeks) * 100
    except ZeroDivisionError:
        ans = 0
    if ans <= 30:
        return 'Active- On Track'
    if ans > 30 and ans <= 70:
        return 'Active- Slow'
    if ans >= 70:
        return 'Inactive' 

"""
Dashboard endpoint, to get all details of a students, courses taken and progress.
"""
class Dashboard(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, format=None):
        students = StudentSerializer(Student.objects.all(), many=True)
        stud = Student.objects.all()
        total_no_students = Student.objects.all().count()
        certified_students = Student.objects.filter(is_certified=True).count()
        courses = Course.objects.all().count()
        progress = progressdata(Student.objects.all())
        
        #students.data[1]['status'] = stat
        for stud in students.data:
            stat = progresstat(stud['id'])
            stud['status'] = stat
        

        content = {
            'message': 'success',
            'error':False,
            'student': students.data,
            'status':status.HTTP_200_OK,
            'total_no_students': total_no_students,
            'total_no_courses': courses,
            'no_certified_students': certified_students,
            'inactive':  progress['in_atcive'],
            'slow': progress['active_slow'],
            'on_track': progress['on_track'],
        }
        return Response(content)




"""
Get Student information endpoint, to get all details of a student, courses taken and progress.
"""
class GetStudentInfo(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, student_id, format=None):
        try:
            stud = Student.objects.get(id=student_id)
        except Student.DoesNotExist:
            return JsonResponse({'message': 'There is no such user','error':True,'status':status.HTTP_400_BAD_REQUEST}, status=status.HTTP_400_BAD_REQUEST)
        studentinfo = StudentSerializer(stud)
        courses =  CourseEnrolledSerializer(CourseEnrolled.objects.filter(student__id=student_id), many=True)
        progress = singleprogress(stud)
        return Response({'message': 'success','error':False,'status':status.HTTP_200_OK,'student':studentinfo.data,'course':courses.data,'inactive':  progress['in_atcive'], 'slow': progress['active_slow'], 'on_track': progress['on_track'],})

