# Generated by Django 2.0.7 on 2020-03-31 21:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20200331_2230'),
    ]

    operations = [
        migrations.AlterField(
            model_name='courseenrolled',
            name='status',
            field=models.IntegerField(choices=[(1, 'active- On track'), (2, 'active- Slow'), (3, 'Inactive')], default=1),
        ),
    ]
