from .models import *
from rest_framework import serializers

class OrganisationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organisation
        fields = '__all__'

class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = '__all__'


class StudentSerializer(serializers.ModelSerializer):
    organisation = serializers.StringRelatedField()
    class Meta:
        model = Student
        fields = '__all__'
    
    def to_representation(self, instance):
        rep = super(StudentSerializer, self).to_representation(instance)
        rep['organisation'] = instance.organisation.name
        return rep

class CourseEnrolledSerializer(serializers.ModelSerializer):
    course = serializers.StringRelatedField()

    class Meta:
        model = CourseEnrolled
        fields = ('id','course','status','course_enrolled_date')
    
    def to_representation(self, instance):
        rep = super(CourseEnrolledSerializer, self).to_representation(instance)
        rep['course'] = instance.course.name
        return rep